const jwt = require('jsonwebtoken');
const config = require('../config');

function verifyToken(req, res, next) {

    const token = req.get('Token');
  // const token = req.headers.token;
  //   console.log(token);
  if (!token)
    return res.status(403).send({auth: false, message: 'No token provided.'});

  jwt.verify(token, config.token.secret, function (err, decoded) {
    if (err)
      return res.status(500).send({auth: false, message: 'Failed to authenticate token.'});

    req.userId = decoded.id;
    next();
  });
}

module.exports = verifyToken;