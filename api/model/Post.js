const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PostSchema = new mongoose.Schema({
    user: {
        type: Schema.Types.ObjectId, ref: 'User'
    },
    title: {
        type: String, required: true
    },
    description: {
        type: String
    },
    price: {
        type: Number
    },
    image: {
        type: String
    },
    category: {
        type: String
    }

});


mongoose.model('Post', PostSchema);

module.exports = mongoose.model('Post');