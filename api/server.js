const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const UsersController = require('./app/usersController');
const PostController = require('./app/postController');

const config = require('./config');

const app = express();

const port = 8000;

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', () => {
  console.log('Database connection');

  app.use('/users', UsersController());

  app.use('/items', PostController());

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  })
});