const express = require('express');
const router = express.Router();
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const Post = require('../model/Post');
const auth = require('../middleware/auth');

const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {
    router.post('/', [auth, upload.single('image')], async function (req, res) {
        const productData = req.body;
        productData.user = req.userId;
        if (req.file) {
            productData.image = req.file.filename;
        } else {
            productData.image = null;
        }
        console.log(productData);
        const post = new Post(productData);

        post.save().then(result => {
            res.send(result);
        }).catch(err => {
            res.status(400).send({error: 'Token not present', message: err.message});
        })

    });

    router.get('/', async function (req, res) {

        try {
            const allTask = await Post.find();

            res.status(200).send(allTask);
        } catch (err) {
            return res.status(401).send({error: 'Post not found', message: err.message})
        }
    });

    router.put('/:id', auth, async function (req, res) {
        try {
            let data = {
                title: req.body.title,
                description: req.body.description,
                category: req.body.category,
                status: req.body.status
            };

            await Post.findByIdAndUpdate(req.params.id, data);

            res.status(200).send({auth: true, data});
        } catch (err) {
            return res.status(401).send({error: 'Post not found', message: err.message})
        }
    });


    router.delete('/:id', auth, async function (req, res) {

        try {
            await Post.findOneAndRemove({_id: req.params.id});

            res.status(200).send({auth: true, message: 'was removed'});
        } catch (err) {
            return res.status(401).send({error: 'Post not found', message: err.message})
        }
    });


    return router;
};

module.exports = createRouter;