const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const nanoid = require('nanoid');

const config = require('../config');
const User = require('../model/User');

const createRouter = () => {


    router.post('/', async function (req, res) {

        const user = new User(req.body);

        user.save()
            .then(user => res.send(user))
            .catch(error => res.status(400).send(error))
    });

    router.post('/sessions', async function (req, res) {

        try {
            const user = await User.findOne({username: req.body.username});

            const passwordIsValid = await bcrypt.compare(req.body.password, user.password);
            if (!passwordIsValid) return res.status(401).send({error: 'password not valid'});

            const token = jwt.sign({id: user._id}, config.token.secret, {
                expiresIn: config.token.lifetime
            });

            user.token = token;
            await user.save();

            res.status(200).send({username: user.username, token: token})
        } catch (err) {
            return res.status(401).send({error: 'Username not found'})
        }
    });

    router.delete('/sessions', async (req, res) => {
        const token = req.get('Token');
        const success = {message: 'Logout success!'};

        if (!token) return res.send(success);

        const user = await User.findOne({token});

        if (!user) return res.send(success);

        user.token = nanoid();
        await user.save();

        return res.send(success);
    });

    return router;
};

module.exports = createRouter;