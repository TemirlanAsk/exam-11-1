import React, {Component} from 'react';
import {Route, Switch} from "react-router-dom";

import Layout from "./containers/Layout/Layout";

import Forums from "./containers/Items/Items";
import Items from "./containers/Items/Items";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import CreateNewItem from "./containers/CreateNewItem/CreateNewItem";

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path="/" exact component={Forums}/>
          <Route path="/register" exact component={Register}/>
          <Route path="/login" exact component={Login}/>
          <Route path="/new_post" exact component={CreateNewItem}/>
          <Route path="/items" exact component={Items}/>
        </Switch>
      </Layout>
    );
  }
}

export default App;
