import axios from '../../axios-api';
import {CREATE_POST_SUCCESS, FETCH_ITEMS_SUCCESS} from "./actionTypes";
import {push} from "react-router-redux";

export const fetchItemsSuccess = items => {
  return {type: FETCH_ITEMS_SUCCESS, items};
};

export const fetchItems = () => {
  return dispatch => {
    axios.get('/items').then(
      response => dispatch(fetchItemsSuccess(response.data))
    );
  }
};

export const createItemsuccess = () => {
  return {type: CREATE_POST_SUCCESS};
};

export const createPost = (postData, token) => {
  return dispatch => {
    return axios.post('/items', postData, {headers: {"Token": token}}).then(
      response => {
          dispatch(createItemsuccess());
          dispatch(push('/'))
      }
    );
  };
};