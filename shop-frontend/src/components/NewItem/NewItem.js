import React, {Component} from 'react';
import axios from 'axios';
import {Button, Col, ControlLabel, Form, FormControl, FormGroup} from "react-bootstrap";

class NewItem extends Component {
  state = {
    title: '',
    description: '',
    price: '',
    image: '',
    category: '',
  };

  submitFormHandler = event => {
    event.preventDefault();

    console.log('new item');

    const formData = new FormData();
    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.onSubmit(formData);
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    });
  };

    saveItemHandler = e => {
        e.preventDefault();
        axios.post('/items', this.state.category).then(response => {
            this.props.history.replace('/');
        });
    };

  render() {
    return (
      <Form horizontal onSubmit={this.submitFormHandler}>
        <FormGroup controlId="postTitle">
          <Col componentClass={ControlLabel} sm={2}>
            Title
          </Col>
          <Col sm={10}>
            <FormControl
              type="text" required
              placeholder="Enter post title"
              name="title"
              value={this.state.title}
              onChange={this.inputChangeHandler}
            />
          </Col>
        </FormGroup>

        <FormGroup controlId="postDescription">
          <Col componentClass={ControlLabel} sm={2}>
            Description
          </Col>
          <Col sm={10}>
            <FormControl
              componentClass="textarea"
              placeholder="Enter description"
              name="description"
              value={this.state.description}
              onChange={this.inputChangeHandler}
            />
          </Col>
        </FormGroup>
          <FormGroup controlId="postPrice">
              <Col componentClass={ControlLabel} sm={2}>
                  Price
              </Col>
              <Col sm={10}>
                  <FormControl
                      type="number" min="0" required
                      placeholder="Enter your price"
                      name="price"
                      value={this.state.price}
                      onChange={this.inputChangeHandler}
                  />
              </Col>
          </FormGroup>


        <FormGroup controlId="postImage">
          <Col componentClass={ControlLabel} sm={2}>
            Image
          </Col>
          <Col sm={10}>
            <FormControl
              type="file"
              name="image"
              onChange={this.fileChangeHandler}
            />
          </Col>
        </FormGroup>


        <FormGroup>
            <select name="category" value={this.state.category} onChange={this.inputChangeHandler}
                    style={{marginLeft: '210px'}}>
                <option>Computers</option>
                <option>Laptops</option>
                <option>Cars</option>
                <option>Phones</option>
                <option>Others</option>
            </select>
          <Col smOffset={2} sm={10}>
            <Button bsStyle="primary" type="submit" style={{marginLeft: '110px'}} >Create Item</Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}

export default NewItem;
