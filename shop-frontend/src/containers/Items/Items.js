import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";
import {fetchItems} from "../../store/actions/items";

import ForumListItem from '../../components/ForumListItem/ForumListItem';

class Items extends Component {
  componentDidMount() {
    this.props.onFetchItem();
  }

  render() {
    return (
      <Fragment>
        <PageHeader>
          All Items
          {/*<Link to="/Forum/new">*/}
            {/*<Button bsStyle="primary" className="pull-right">*/}
              {/*Add Forum*/}
            {/*</Button>*/}
          {/*</Link>*/}
        </PageHeader>

        {this.props.items.map(forum => (
          <ForumListItem
            key={forum._id}
            id={forum._id}
            title={forum.title}
            price={forum.price}
            description={forum.description}
            image={forum.image}
          />
        ))}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    items: state.items.items
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchItem: () => dispatch(fetchItems())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Items);