import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import ProductForm from "../../components/NewItem/NewItem";
import {createPost} from "../../store/actions/items";

class CreateNewItem extends Component {
  createProduct = productData => {
      for(let key of productData.entries()) {
          console.log(key)
      }
      productData.append('user', this.props.user._id);
    this.props.onPostCreated(productData, this.props.user.token).then(() => {
      this.props.history.push('/');
    });
  };

  render() {
    return (
      <Fragment>
        <PageHeader>Create New Item</PageHeader>
        <ProductForm onSubmit={this.createProduct} />
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
    return {
        user: state.users.user
    }
};
const mapDispatchToProps = dispatch => {
  return {
    onPostCreated: (productData, token)=> {
      return dispatch(createPost(productData, token))
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateNewItem);